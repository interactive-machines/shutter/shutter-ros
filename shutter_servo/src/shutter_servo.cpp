#include <tf2_eigen/tf2_eigen.h>
#include "shutter_servo/shutter_servo.h"

static const std::string LOGNAME = "shutter_servo";

namespace shutter_servo
{
ShutterServo::ShutterServo(const ros::NodeHandle& nh,
                       const planning_scene_monitor::PlanningSceneMonitorPtr& planning_scene_monitor)
  : nh_(nh)
  , planning_scene_monitor_(planning_scene_monitor)
{
  // Get incoming command topic name
  std::string goal_positions_topic;
  nh_.param<std::string>("goal_positions_topic", goal_positions_topic, "servo/joint_command");

  // Get expected publishing rate
  // publishing rate

  // Use the C++ interface that Servo provides
  servo_ = std::make_unique<moveit_servo::Servo>(nh_, planning_scene_monitor_);
  servo_->start();

  // Setup ROS interfaces
  joint_deltas_pub_ = nh_.advertise<control_msgs::JointJog>("servo/delta_joint_cmds", 1, true);
  joint_sub_ = nh_.subscribe(goal_positions_topic, 1, &ShutterServo::joint_command_callback, this);
  servo_status_sub_ = nh_.subscribe("servo/status", 1, &ShutterServo::status_callback, this);
  joint_update_timer_ = nh_.createTimer(ros::Duration(0.067), &ShutterServo::joint_update_callback, this, false, false);
}

void ShutterServo::joint_update_callback(const ros::TimerEvent& event)
{
  control_msgs::JointJog joint_deltas;
  joint_deltas.joint_names = {"joint_1", "joint_2", "joint_3", "joint_4"};

  // lookup current joints values
  auto current_state = planning_scene_monitor_->getStateMonitor()->getCurrentState();
  current_state->copyJointGroupPositions(PLANNING_GROUP, current_joints);

  if (std::any_of(current_joints.begin(), current_joints.end(), [](double position){ return std::isnan(position); }))
  {
    ROS_ERROR_STREAM_THROTTLE(1, "current_joints contains NaN, blocking!");
    return;
  }

  for (int i = 0; i < 4; i++)
  {
    joint_deltas.velocities.push_back((goal_positions[i] - current_joints[i]) / 0.06);
  }

  joint_deltas.header.stamp = ros::Time::now();
  if (!(status_ == moveit_servo::StatusCode::HALT_FOR_SINGULARITY
        || status_ == moveit_servo::StatusCode::HALT_FOR_COLLISION))
  {
    joint_deltas_pub_.publish(joint_deltas);
  }
}

void ShutterServo::start()
{
  while (std::any_of(goal_positions.begin(), goal_positions.end(), [](double position){ return std::isnan(position); }))
  {
    ROS_WARN_STREAM_THROTTLE(1, "goal_positions contains NaN, blocking!");
  }

  joint_update_timer_.start();
  servo_->setPaused(false);
}

void ShutterServo::stop()
{
  joint_update_timer_.stop();
  servo_->setPaused(true);
}

void ShutterServo::joint_command_callback(const std_msgs::Float64MultiArray& msg)
{
  goal_positions.clear();
  goal_positions = msg.data;
}

void ShutterServo::status_callback(const std_msgs::Int8ConstPtr& msg)
{
  moveit_servo::StatusCode latest_status = static_cast<moveit_servo::StatusCode>(msg->data);
  if (latest_status != status_)
  {
    status_ = latest_status;
    const auto& status_str = moveit_servo::SERVO_STATUS_CODE_MAP.at(status_);
    ROS_DEBUG_STREAM_THROTTLE(1, "Servo status: " << status_str);
  }
}
}
