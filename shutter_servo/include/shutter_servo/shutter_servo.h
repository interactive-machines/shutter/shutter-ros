#include <memory>
#include <vector>

#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Int8.h>
#include <control_msgs/JointJog.h>
#include <moveit_servo/servo.h>
#include <moveit_servo/status_codes.h>

namespace shutter_servo
{
class ShutterServo
{
public:
  ShutterServo(const ros::NodeHandle& nh,
             const planning_scene_monitor::PlanningSceneMonitorPtr& planning_scene_monitor);

  void start();
  void stop();

private:
  const std::string PLANNING_GROUP = "shutter_arm";

  planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor_;
  std::unique_ptr<moveit_servo::Servo> servo_;

  std::vector<double> goal_positions{std::nan("1"), std::nan("1"), std::nan("1"), std::nan("1")}; // initialise with NaN
  std::vector<double> current_joints{std::nan("1"), std::nan("1"), std::nan("1"), std::nan("1")}; // initialise with NaN
  moveit_servo::StatusCode status_ = moveit_servo::StatusCode::INVALID;

  ros::NodeHandle nh_;
  ros::Publisher joint_deltas_pub_;
  ros::Subscriber joint_sub_;
  ros::Subscriber servo_status_sub_;
  ros::Timer joint_update_timer_;

  void joint_update_callback(const ros::TimerEvent& event);
  void joint_command_callback(const std_msgs::Float64MultiArray& msg);
  void status_callback(const std_msgs::Int8ConstPtr& msg);
};
}
