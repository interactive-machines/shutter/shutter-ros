#!/usr/bin/env python3

import os
from pathlib import Path
import shutil
import subprocess

import gdown
import rospy


class UnitySimulationManager():
    """
    Manager node to setup, start and stop a Shutter simulation built with Unity
    """
    def __init__(self):
        """
        Start UnitySim after checking that UnitySim exists and contains correct version.
        """
        self.shutter_ros_path = Path(__file__).resolve().parents[2]
        self.unity_path = self.shutter_ros_path / 'UnitySim'
        self.version_fpath = self.unity_path / 'version.txt'
        self.simulation_fpath = self.unity_path / 'Shutter_MoveIt.x86_64'
        self.gdrive_id = '1DhTMfE0y1KJF1Wz3XbZ5AA1EGS_ZTEhk'

        rospy.init_node('simulation_manager')
        if not self.check_install():
            rospy.logfatal("Check for Unity simulation installation failed.")
            raise rospy.ROSInterruptException
        executable_args = [self.simulation_fpath]
        headless = rospy.get_param('/headless', False)
        if headless:
            executable_args.extend(['-batchmode',
                                    '-nographics'])
        unity_simulation_process = subprocess.Popen(executable_args)
        rospy.spin()
        unity_simulation_process.terminate()

    def check_install(self):
        """
        Check that UnitySim is extracted to expected directory and has correct version.
        Attempts to reinstall from scratch if version is old.
        """
        if not self.unity_path.is_dir():
            rospy.logwarn(f'{self.unity_path} is NOT an existing directory; assuming install is missing.')
            if self.install_unity_sim():
                return True
        if self.check_version():
            return True
        rospy.logwarn('Attempting to remove and re-install UnitySim...')
        try:
            shutil.rmtree(self.unity_path)
            rospy.logdebug(f'Removal of {self.unity_path} succeeded; attemping to install UnitySim.')
            if self.install_unity_sim():
                return True
        except Exception as e:
            rospy.logerr(f'{e}')
        return False

    def install_unity_sim(self):
        """
        :return: True if no exceptions were raised, False otherwise
        Install the UnitySim package from Google Drive.
        """
        try:
            tarfile_fpath = self.shutter_ros_path / 'UnitySim.tgz'
            simulation_url = f'https://drive.google.com/uc?id={self.gdrive_id}'
            gdown.download(simulation_url, str(tarfile_fpath), quiet=False)
            rospy.logdebug('Finished downloading simulation package, unzipping...')
            gdown.extractall(str(tarfile_fpath))
            self.version_fpath.write_text(self.gdrive_id)
            os.remove(tarfile_fpath)
            rospy.loginfo(f'Download and extraction to {self.unity_path} succeeded.')
            return True
        # gdown does not define any exceptions but raises Runtime and Value Errors
        except (RuntimeError, ValueError) as e:
            rospy.logerr(f'{e}')
            return False

    def check_version(self):
        """
        :return: True if the version is correct, False otherwise
        Check if the version matches the Google Drive ID of the latest commit.
        """
        with self.version_fpath.open() as f:
            version_line = f.readline().rstrip('\n')
            if version_line == self.gdrive_id:
                return True
            rospy.logwarn(f'Found Google Drive ID: {version_line} did not match expected ID {self.gdrive_id}.')
            return False


if __name__ == '__main__':
    try:
        UnitySimulationManager()
    except rospy.ROSInterruptException:
        pass
