.. title:: Shutter Description Package

.. include:: ../../../shutter_description/README.md
   :parser: myst_parser.sphinx_

----

.. include:: ../../../shutter_description/ACKNOWLEDGEMENTS.md
   :parser: myst_parser.sphinx_
