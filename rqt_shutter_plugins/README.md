# RQT Shutter Plugins

**NOTE**: the following plugin(s) are experimental.

To start the provided perspective for [rqt](http://wiki.ros.org/rqt/) to control Shutter's joints:

```bash
$ roslaunch rqt_shutter_plugins shutter_rqt.launch [simulation:=true]
```

To send joint commands to the robot, follow the steps:

1. **This item only needs to be completed when controlling a physical robot.**
   Controller management is not used for the Unity-built simulation.
   
   Start the `joint_group_controller` controller using the Controller Manager plugin (lower left panel).
   Right-click the controller item in the menu, then click on "Start".
   The controller item will have a green bubble if the controller starts successfully.

   Note that you may need to activate the namespace `/controller_manager` from the dropdown menu first.

1. Switch on the `Enable Shutter control` checkbox.
   The joint sliders will be greyed out (inactive) until the checkbox is active.

1. Send new joint position goals to the robot by clicking or moving the sliders.
   Clicking the sliders increments/decrements the joint position value by **3** degrees.
   Dragging the sliders does NOT update the joints values until the slider is released.



The three main steps above are illustrated in the following movie:

![controlling Shutter with rqt](../images/shutter_rqt.gif){w=500px align=center}