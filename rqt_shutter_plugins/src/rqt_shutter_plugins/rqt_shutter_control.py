import math
from pathlib import Path

import rospy
import rospkg
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64MultiArray

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtCore import Signal, Slot
from python_qt_binding.QtWidgets import QWidget, QSlider, QLabel, QCheckBox, QGroupBox

class ShutterControlPlugin(Plugin):
    """Shutter Arm Control Plugin"""
    js_signal = Signal(object)  # signal for joint state update

    def __init__(self, context):
        super(ShutterControlPlugin, self).__init__(context)
        self.setObjectName('ShutterControlPlugin')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())

        # Get joint params
        self.joint_limits = {}
        self.get_joint_params()
        if not args.quiet:
            print("Joint Limits:", self.joint_limits)

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = Path(rospkg.RosPack().get_path('rqt_shutter_plugins')) / 'resources/shutter_control.ui'

        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('ShutterControlUI')

        self.control_check = self._widget.findChild(QCheckBox, "checkBox_control")
        self.control_box = self._widget.findChild(QGroupBox, "groupBox_control")
        self.control_box.setEnabled(False)
        self.control_check.stateChanged.connect(self.toggle_control_box)

        self.slider_j1_label = self._widget.findChild(QLabel, 'label_slider_j1')
        self.slider_j2_label = self._widget.findChild(QLabel, 'label_slider_j2')
        self.slider_j3_label = self._widget.findChild(QLabel, 'label_slider_j3')
        self.slider_j4_label = self._widget.findChild(QLabel, 'label_slider_j4')

        self.slider_j1 = self._widget.findChild(QSlider, 'slider_j1')
        self.slider_j2 = self._widget.findChild(QSlider, 'slider_j2')
        self.slider_j3 = self._widget.findChild(QSlider, 'slider_j3')
        self.slider_j4 = self._widget.findChild(QSlider, 'slider_j4')
        self.setup_sliders()

        # Show _widget.windowTitle on left-top of each plugin (when
        # it's set in _widget). This is useful when you open multiple
        # plugins at once. Also if you open multiple instances of your
        # plugin at once, these lines add number to make it easy to
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

        # Publisher
        self.joints = [0.0, 0.0, 0.0, 0.0]
        self.joint_command_pub = rospy.Publisher('/joint_group_controller/command', Float64MultiArray, queue_size=5)

        # Subscribers
        self.sub = rospy.Subscriber('/joint_states', JointState, self.joint_states_callback)

        # Last set of connections
        self.js_signal.connect(self.record_joint_states)

        self.slider_j1.valueChanged.connect(
            lambda x: self.update_slider_label(self.slider_j1_label, 1, x))
        self.slider_j2.valueChanged.connect(
            lambda x: self.update_slider_label(self.slider_j2_label, 2, x))
        self.slider_j3.valueChanged.connect(
            lambda x: self.update_slider_label(self.slider_j3_label, 3, x))
        self.slider_j4.valueChanged.connect(
            lambda x: self.update_slider_label(self.slider_j4_label, 4, x))

    def publish_joint_command(self, joint_id, value):
        """
        Publish joint command
        :param joint_id: joint ID, one-index
        :param value: slider value (in degrees)
        """
        radians = value * math.pi / 180.0
        self.joints[joint_id - 1] = radians  # lists are zero-indexed but joints are one-indexed
        self.joint_command_pub.publish(Float64MultiArray(data=self.joints))

    def joint_states_callback(self, data):
        """Send the data to the GUI."""
        if not self.control_box.isEnabled():
            self.joints = list(data.position)
            self.js_signal.emit(data)

    @Slot(object)
    def record_joint_states(self, data):
        """Reads the /joint_states topic to update the position and velocity labels"""
        def set_state(joint_name, slider, label):
            value = data.position[data.name.index(joint_name)] * 180.0 / math.pi
            slider.setValue(value)
            label.setText(str(round(value)))

        set_state("joint_1", self.slider_j1, self.slider_j1_label)
        set_state("joint_2", self.slider_j2, self.slider_j2_label)
        set_state("joint_3", self.slider_j3, self.slider_j3_label)
        set_state("joint_4", self.slider_j4, self.slider_j4_label)

    def toggle_control_box(self, state):
        if state > 0:
            self.control_box.setEnabled(True)
        else:
            self.control_box.setEnabled(False)

    def update_slider_label(self, label, joint_id, value):
        """
        Update the label of a slider
        :param label: label object
        :param joint_id: joint ID, one-index
        :param value: slider value
        """
        label.setText(str(value))
        if self.control_box.isEnabled():
            self.publish_joint_command(joint_id, value)

    def get_joint_params(self):
        """
        Get joint params from ROS environment.
        Mimics soft joint limits defined in shutter_hardware_interface pkg.
        """
        self.joint_limits['joint_1'] = (-148, 148)
        self.joint_limits['joint_2'] = (-88, 88)
        self.joint_limits['joint_3'] = (-88, 88)
        self.joint_limits['joint_4'] = (-88, 88)

    def setup_sliders(self):
        """Helper function to setup the sliders for controlling the servos."""
        def configure_slider(slider, min_value, max_value, current_value, label):
            slider.setMinimum(min_value)
            slider.setMaximum(max_value)
            slider.setValue(current_value)
            slider.setTickPosition(QSlider.TicksBelow)
            slider.setTickInterval(3)
            slider.setPageStep(3)
            slider.setTracking(False)
            label.setText(str(current_value))

        configure_slider(self.slider_j1, self.joint_limits['joint_1'][0], self.joint_limits['joint_1'][1], 0,
                         self.slider_j1_label)
        configure_slider(self.slider_j2, self.joint_limits['joint_2'][0], self.joint_limits['joint_2'][1], 0,
                         self.slider_j2_label)
        configure_slider(self.slider_j3, self.joint_limits['joint_3'][0], self.joint_limits['joint_3'][1], 0,
                         self.slider_j3_label)
        configure_slider(self.slider_j4, self.joint_limits['joint_4'][0], self.joint_limits['joint_4'][1], 0,
                         self.slider_j4_label)

    def shutdown_plugin(self):
        """Clean up. Unregister subscribers."""
        self.sub.unregister()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass
