#!/usr/bin/env python
# Node to command the eyes on the robot based on a desired 3D gaze direction
import rospy
from geometry_msgs.msg import PointStamped
import tf2_geometry_msgs
import tf2_ros
from shutter_face_ros.msg import PupilsLocation
from shutter_face.face_params import fp_v1 as fp

class GazeMaster:
    """
    Gaze master node. Converts 3D gaze directions into 2D pupil positions.
    """
    def __init__(self):
        # init node
        rospy.init_node('gaze_master', anonymous=False)

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        # eye parameters (we assume both eyes have the same parameters)
        self.fx = rospy.get_param("~fx", fp.fx)
        self.fy = rospy.get_param("~fy", fp.fy)
        rospy.loginfo("fx={}, fy={}".format(self.fx, self.fy))

        # get static transforms from head to eyes (this way we only
        self.left_trans = None
        self.right_trans = None
        while self.left_trans is None and self.right_trans is None:

            if rospy.is_shutdown():
                return

            try:
                self.left_trans = \
                    self.tfBuffer.lookup_transform("left_eye", "head_link", rospy.Time(), rospy.Duration(1.0))
                self.right_trans = \
                    self.tfBuffer.lookup_transform("head_link", "right_eye", rospy.Time(), rospy.Duration(1.0))

            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                rospy.logwarn("Waiting for left/right eye transforms from the head_link. Is the robot's URDF being broadcasted?")
                rospy.sleep(3)

        rospy.loginfo("Got eye static transforms")

        # publishers
        self.pupils_pub = rospy.Publisher("gaze/pupils_location", PupilsLocation, queue_size=5)
        self.left_pub = rospy.Publisher("gaze/left_coordinate_points", PointStamped, queue_size=5)

        # subscribers
        rospy.Subscriber("gaze/coordinate_points", PointStamped, self.callback)

        # do nothing.. just wait for gaze commands
        rospy.spin()

    def callback(self, point_msg):

        try:
            trans = \
                self.tfBuffer.lookup_transform("head_link", point_msg.header.frame_id, rospy.Time(0), rospy.Duration(0.1))

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logerr("Failed to transform point in {} frame to head_link:\n{}".format(point_msg.header.frame_id, e))
            return

        # compute the target location from each of the eyes
        target_point = tf2_geometry_msgs.do_transform_point(point_msg, trans)
        left_point = tf2_geometry_msgs.do_transform_point(target_point, self.left_trans)
        right_point = tf2_geometry_msgs.do_transform_point(target_point, self.right_trans)

        self.left_pub.publish(left_point)

        # convert the 3D target points into 2D pixel coordinates
        left_projection = self.compute_2d_projection(left_point.point)
        right_projection = self.compute_2d_projection(right_point.point)
        # print ("target {} {} {} - left {} {} {} - projection {} {}".format(target_point.point.x,
        #                                                                    target_point.point.y,
        #                                                                    target_point.point.z,
        #                                                                    left_point.point.x,
        #                                                                    left_point.point.y,
        #                                                                    left_point.point.z,
        #                                                                    left_projection[0],
        #                                                                    left_projection[1]))

        # finally publish the result
        msg = PupilsLocation()
        msg.header = point_msg.header
        msg.left_eye.x = left_projection[0]
        msg.left_eye.y = left_projection[1]
        msg.right_eye.x = right_projection[0]
        msg.right_eye.y = right_projection[1]
        self.pupils_pub.publish(msg)


    def compute_2d_projection(self, point):
        """
        Helper function to compute 2D projection of a point
        :param point: 3D point seen from the center of the camera
        :return: 2D point
        """
        px = self.fx * point.x / -point.z # we flip z because the eyes cameras are in the 3D model poining backwards to match the Qt coordinate frame
        py = self.fy * point.y / -point.z
        return px, py


if __name__ == '__main__':
    try:
        GazeMaster()
    except rospy.ROSInterruptException:
        pass
