#!/usr/bin/env python3

import rospy
import actionlib

from control_msgs.msg import FollowJointTrajectoryAction
from std_msgs.msg import Float64MultiArray


class UnityTrajectoryController():

    def __init__(self, name):
        self.action_name = name
        self.joints_pub = rospy.Publisher("/unity_joint_group_controller/command", Float64MultiArray, queue_size=1, latch=True)
        self.action_server = actionlib.SimpleActionServer("/unity_trajectory_controller/follow_joint_trajectory", FollowJointTrajectoryAction, execute_cb=self.trajectory_cb, auto_start=False)
        self.action_server.start()

    def trajectory_cb(self, goal):
        success = True
        previous_waypoint = rospy.Duration(0, 0)
        for waypoint in goal.trajectory.points:
            if self.action_server.is_preempt_requested():
                rospy.logdebug(f'{self.action_name}: Preempted')
                self.action_server.set_preempted()
                success = False
                break
            joints_goal = Float64MultiArray()
            joints_goal.data = waypoint.positions
            self.joints_pub.publish(joints_goal)
            rospy.sleep(waypoint.time_from_start - previous_waypoint)
            previous_waypoint = waypoint.time_from_start
        if success:
            rospy.logdebug(f'{self.action_name}: Succeeded')
            self.action_server.set_succeeded()


if __name__ == '__main__':
    rospy.init_node('unity_trajectory_controller')
    trajectory_server = UnityTrajectoryController(rospy.get_name())
    rospy.spin()
