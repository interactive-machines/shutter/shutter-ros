**Acknowledgements:** The arm model is based on the [widowx_arm_description](http://ros.org/wiki/widowx_arm_description) 
package by Robotnik. The Real Sense D435 camera model is based on 
the [official realsense2_camera package](https://github.com/intel-ros/realsense/tree/development/realsense2_camera) by Intel. 
For Shutter v0.1, the ZED camera model is based on 
the [racecar_description](https://github.com/mit-racecar/racecar-simulator) package by MIT.
