# Shutter Opt Control Package

Motion control scripts using optimization.

## Launch file

The functionality described below can also be orchestrated with a launch file:

```console
$ roslaunch shutter_opt_control teleop.launch simulation:=true
```

Note that a PS3 controller is expected to be accessible on device: `/dev/input/js0` which can be changed with the roslaunch argument `device`.

An optional RViz configuration can also be loaded through the launch file:

```console
$ roslaunch shutter_opt_control teleop.launch rviz:=true
```

When controlling the real robot, the joints should be manually moved into a good configuration **before** starting the launch file.
Otherwise, either MoveIt Servo or the ROS Control joint limits interface may detect a potential self-collision and block any motion.

## Quick Start: Control Yaw and Pich

Start the robot driver. For example, in simulation that would correspond to:
```console
$ roslaunch shutter_bringup shutter.launch simulation:=true
```

Run the controller:

```console
$ rosrun shutter_opt_control optimize_joints_towards_target.py
```

Then you can send requests to direct the robot's camera in a particular direction by publising PoseStamped messages to the `/target` topic. This functionality can be tested with the teleop_target.py node that connects to a PS3 controller:

```console
$ rosrun joy joy_node _dev:=/dev/input/js<number> _autorepeat_rate:=30  # get commands from ps3 controller
$ rosrun shutter_opt_control teleop_target.py  # run node that publishes targets
```

You should then be able to control the target (red) in front of the robot with
the PS3 controller.

![Shutter following a virtual target](../images/shutter_moving.gif){w=300px align=center}


## Halo Teleoperation

This package also supports teleoperation from the target's perspective, which is particularly helpful when evaluating "lookat" motion generation in simulation.

To test this functionality, connect a PS3 controller or equivalent gamepad and run:

```console
$ roslaunch shutter_opt_control halo.launch [rviz:=true] [simulation:=true]
```

The joystick mapping is:

| Side  | Direction  | Control              |
|-------|------------|----------------------|
| left  | up/down    | forward translation  |
| left  | left/right | lateral translation  |
| right | up/down    | vertical translation |
| right | left/right | camera pan           |

Additionally, pressing the "cross" button will reset the target to its starting pose.

Teleoperation from the target's perspective is termed "halo teleoperation", both in reference to the iconography of a floating crown and to the classic video game.