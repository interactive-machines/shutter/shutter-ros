#!/usr/bin/env python3
import math

import rospy
import tf2_ros
from tf.transformations import euler_from_quaternion, quaternion_from_euler, quaternion_multiply
from tf2_geometry_msgs import do_transform_pose

from geometry_msgs.msg import PoseStamped, Quaternion, TransformStamped
from sensor_msgs.msg import Joy
from visualization_msgs.msg import Marker, MarkerArray

from teleop_target import Boundary, SimulatedObject

class TeleopHalo():

    PS3_AXIS_STICK_LEFT_LEFTWARDS = 0
    PS3_AXIS_STICK_LEFT_UPWARDS = 1
    PS3_AXIS_STICK_RIGHT_LEFTWARDS = 3
    PS3_AXIS_STICK_RIGHT_UPWARDS = 4
    PS3_L1 = 2
    PS3_R1 = 5
    PS3_BUTTON_CROSS = 0
    PS3_BUTTON_CIRCLE = 1

    def __init__(self):
        """
        Main function. Publishes the target at a constant frame rate.
        """
        kinect_bounds = Boundary(0.3, 2.5, -5, 5, -1, 2)
        self.object = SimulatedObject(boundary=kinect_bounds)
        self.object.yaw = math.pi
        self.command_frame = "halo"
        self.offset_rot = quaternion_from_euler(0, math.pi, math.pi)
        self.rotation_scale = 0.06
        self.translation_scale = 0.02

        rospy.init_node('generate_halo', anonymous=True)
        self.br = tf2_ros.TransformBroadcaster()
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0))  # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        vector_pub = rospy.Publisher('/target', PoseStamped, queue_size=5)
        marker_pub = rospy.Publisher('/target_marker', Marker, queue_size=5)
        bounded_pub = rospy.Publisher('/bounded_markers', MarkerArray, queue_size=5)

        rospy.Subscriber('joy', Joy, self.joy_callback, queue_size=5)

        hz = 15
        rate = rospy.Rate(hz)
        while not rospy.is_shutdown():
            # publish the location of the target as a Vector3Stamped
            pose_msg = self.object.construct_pose_msg()
            vector_pub.publish(pose_msg)
            self.update_frame(pose_msg)

            # publish a marker to visualize the target in RViz
            marker_msg = self.object.construct_marker_msg(pose_msg)
            marker_pub.publish(marker_msg)

            # check scene bounds
            if self.object.check_bounds():
                bounded_marker_msg = self.object.construct_marker_msg(pose_msg)
                bounded_pub.publish(MarkerArray(markers=[bounded_marker_msg]))
            else:
                bounded_pub.publish(MarkerArray(markers=[]))

            self.object.update(1.0/hz)

            # sleep to keep the desired publishing rate
            rate.sleep()

    def update_frame(self, pose_msg):
        target_rot = [pose_msg.pose.orientation.x,
                      pose_msg.pose.orientation.y,
                      pose_msg.pose.orientation.z,
                      pose_msg.pose.orientation.w]
        new_rot = quaternion_multiply(target_rot, self.offset_rot)
        t = TransformStamped()
        t.header = pose_msg.header
        t.child_frame_id = self.command_frame
        t.transform.translation = pose_msg.pose.position
        t.transform.rotation = Quaternion(new_rot[0], new_rot[1], new_rot[2], new_rot[3])
        self.br.sendTransform(t)

    def transform_pose(self, pose, source_frame, dest_frame):
        try:
            new_transform = self.tf_buffer.lookup_transform(source_frame,
                                                            dest_frame,
                                                            pose.header.stamp,
                                                            rospy.Duration(0.07))
        except (tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as e:
            rospy.logerr(e)
            return
        new_pose_msg = do_transform_pose(pose, new_transform)
        return new_pose_msg

    def joy_callback(self, msg):
        """
        Joy Callback
        :param msg: joy message
        """
        # prioritise reset buttons
        if msg.buttons[TeleopHalo.PS3_BUTTON_CROSS] == 1:
            self.object.x = 1.5
            self.object.y = 0.0
            self.object.z = 0.5
            self.object.roll = 0.0
            self.object.pitch = 0.0
            self.object.yaw = math.pi
            return

        if msg.buttons[TeleopHalo.PS3_BUTTON_CIRCLE] == 1:
            self.object.clip_to_bounds()
            return

        command_pose = self.transform_pose(self.object.construct_pose_msg(), self.object.frame, self.command_frame)
        if command_pose is None:
            return  # transform_pose method already outputs to stderr

        # compute orientation changes relative to the target
        command_angles = euler_from_quaternion([command_pose.pose.orientation.x,
                                                command_pose.pose.orientation.y,
                                                command_pose.pose.orientation.z,
                                                command_pose.pose.orientation.w])
        command_yaw = command_angles[2] - (msg.axes[TeleopHalo.PS3_AXIS_STICK_RIGHT_LEFTWARDS] * self.rotation_scale)
        new_rot = quaternion_from_euler(command_angles[0], command_angles[1], command_yaw)
        command_pose.pose.orientation = Quaternion(new_rot[0], new_rot[1], new_rot[2], new_rot[3])

        command_pose.pose.position.x += msg.axes[TeleopHalo.PS3_AXIS_STICK_LEFT_UPWARDS] * self.translation_scale
        command_pose.pose.position.y -= msg.axes[TeleopHalo.PS3_AXIS_STICK_LEFT_LEFTWARDS] * self.translation_scale
        command_pose.pose.position.z -= msg.axes[TeleopHalo.PS3_AXIS_STICK_RIGHT_UPWARDS] * self.translation_scale
        base_command_pose = self.transform_pose(command_pose, self.command_frame, self.object.frame)
        if base_command_pose is None:
            return  # transform_pose method already outputs to stderr
        self.object.update_from_pose_msg(base_command_pose)


if __name__ == '__main__':
    try:
        TeleopHalo()
    except rospy.ROSInterruptException:
        pass
