#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray
import tf2_ros
import tf.transformations as tft
import tf2_geometry_msgs
import numpy as np
from urdf_parser_py.urdf import URDF
from scipy.optimize import least_squares
from std_msgs.msg import Bool
from controller_manager_msgs.srv import SwitchController

class LookAtTargetOptNode():

    def __init__(self):
        rospy.init_node('lookat_opt_target')

        # params
        self.base_link = rospy.get_param("~base_link", "base_link")
        self.biceps_link = rospy.get_param("~biceps_link", "biceps_link")
        self.camera_link = rospy.get_param("~camera_link", "camera_color_optical_frame")
        self.move = rospy.get_param("~move_init_value", True)
        simulation = rospy.get_param("~simulation", False)

        # joint values
        self.joint1 = None
        self.joint3 = None

        # get robot model
        self.robot = URDF.from_parameter_server()
        #print robot

        # joint publisher
        self.joint_group_pub = rospy.Publisher('/optimize_joints_towards_target/command', Float64MultiArray, queue_size=1)

        # tf subscriber
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0))  # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        # joint subscriber
        rospy.Subscriber('/joint_states', JointState, self.joints_callback, queue_size=5)
        rospy.Subscriber('/target', PoseStamped, self.target_callback, queue_size=5)
        rospy.Subscriber('/move_towards_target', Bool, self.move_callback, queue_size=5)

        # switch controller if running on physical robot
        if not simulation:
            rospy.logwarn('looking for service')
            rospy.wait_for_service('/controller_manager/switch_controller')
            switch_controller = rospy.ServiceProxy('/controller_manager/switch_controller', SwitchController)
            if not switch_controller(start_controllers=['joint_group_controller'], stop_controllers=['follow_trajectory_controller'], strictness=SwitchController._request_class.BEST_EFFORT):
                rospy.logerr("Unable to switch to /joint_group_controller; is shutter_hardware_interface started?")

        rospy.spin()


    def move_callback(self, msg):
        self.move = msg.data
        rospy.logwarn("Motion is: {}".format(self.move))


    def joints_callback(self, msg):
        """
        Joints callback
        :param msg:
        :return:
        """
        joint1_idx = -1
        joint3_idx = -1
        for i in range(len(msg.name)):
            if msg.name[i] == 'joint_1':
                joint1_idx = i
            elif msg.name[i] == 'joint_3':
                joint3_idx = i
        assert joint1_idx >= 0 and joint3_idx > 0, \
            "Missing joints from joint state! joint1 = {}, joint3 = {}".format(joint1_idx, joint3_idx)
        self.joint1 = msg.position[joint1_idx]
        self.joint3 = msg.position[joint3_idx]


    def make_joint_rotation(self, angle, rotation_axis='x'):
        """
        Make rotation matrix for joint (assumes that joint angle is zero)
        :param angle: joint angle
        :param rotation_axis: rotation axis as string or vector
        :return: rotation matrix
        """
        # set axis vector if input is string
        if not isinstance(rotation_axis,list):
            assert rotation_axis in ['x', 'y', 'z'], "Invalid rotation axis '{}'".format(rotation_axis)
            if rotation_axis == 'x':
                axis = (1.0, 0.0, 0.0)
            elif rotation_axis == 'y':
                axis = (0.0, 1.0, 0.0)
            else:
                axis = (0.0, 0.0, 1.0)
        else:
            axis = rotation_axis
        # make rotation matrix
        R = tft.rotation_matrix(angle, axis)
        return R


    def transform_msg_to_T(self, trans):
        """
        Convert TransformStamped message to 4x4 transformation matrix
        :param trans: TransformStamped message
        :return:
        """
        # extract relevant information from transform
        q = [trans.transform.rotation.x,
             trans.transform.rotation.y,
             trans.transform.rotation.z,
             trans.transform.rotation.w]
        t = [trans.transform.translation.x,
             trans.transform.translation.y,
             trans.transform.translation.z]
        # convert to matrices
        Rq = tft.quaternion_matrix(q)
        Tt = tft.translation_matrix(t)
        return np.dot(Tt, Rq)


    def target_in_camera_frame(self, angles, target_pose, T1, T2):
        """

        :param angle:
        :param target_pose:
        :param T:
        :return:
        """

        # make transform for joint 1
        R1 = self.make_joint_rotation(angles[0], rotation_axis=self.robot.joints[1].axis)

        # make transform for joint 3
        R2 = self.make_joint_rotation(angles[1], rotation_axis=self.robot.joints[3].axis)

        # transform target to camera_link
        p = np.array([[target_pose[0], target_pose[1], target_pose[2], 1.0]]).transpose()
        result = np.dot(np.dot(np.dot(T2,R2), np.dot(T1, R1)), p)

        return result[0:2].flatten()


    def target_callback(self, msg):
        """
        Target callback
        :param pose_msg:
        :return:
        """
        if self.joint1 is None:
            rospy.logwarn("Joint 1 is unknown. Waiting to receive joint states.")
            return

        if not self.move:
            return

        # transform the target to baselink if it's not in that frame already
        if msg.header.frame_id != self.base_link:
            try:
                transform = self.tf_buffer.lookup_transform(self.base_link,
                                                            msg.header.frame_id,  # source frame
                                                            msg.header.stamp,
                                                            # get the transform at the time the pose was generated
                                                            rospy.Duration(0.1))  # wait for 1 second
                pose_transformed = tf2_geometry_msgs.do_transform_pose(msg, transform)
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                rospy.logerr(e)
                return
        else:
            pose_transformed = msg

        p = [pose_transformed.pose.position.x,
             pose_transformed.pose.position.y,
             pose_transformed.pose.position.z]

        # get transform from base link to camera link
        try:
            transform = self.tf_buffer.lookup_transform(self.biceps_link,
                                                        self.base_link,  # source frame
                                                        msg.header.stamp,
                                                        # get the transform at the time the pose was generated
                                                        rospy.Duration(0.1))  # wait for 1 second
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logerr(e)
            return

        T1 = self.transform_msg_to_T(transform)


        try:
            transform = self.tf_buffer.lookup_transform(self.camera_link,
                                                        self.biceps_link,  # source frame
                                                        msg.header.stamp,
                                                        # get the transform at the time the pose was generated
                                                        rospy.Duration(0.1))  # wait for 1 second
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logerr(e)
            return

        T2 = self.transform_msg_to_T(transform)

        # minimize
        x0 = [-np.arctan2(p[1], p[0]), 0.0]
        res = least_squares(self.target_in_camera_frame, x0, bounds=([-np.pi, -np.pi*0.5], [np.pi, np.pi*0.5]), args=(p, T1, T2))
        #print "result: {}, cost: {}".format(res.x, res.cost)

        # publish command
        self.joint_group_pub.publish(Float64MultiArray(data=[self.joint1 - res.x[0], 0.0, self.joint3 - res.x[1], 0.0]))


if __name__ == '__main__':
    try:
        node = LookAtTargetOptNode()
    except rospy.ROSInterruptException:
        pass
