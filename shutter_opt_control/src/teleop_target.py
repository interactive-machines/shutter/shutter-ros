#!/usr/bin/env python3
from collections import namedtuple

import numpy as np
import rospy
from tf.transformations import euler_from_quaternion, quaternion_from_euler

from geometry_msgs.msg import PoseStamped, Quaternion
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool
from visualization_msgs.msg import Marker

Boundary = namedtuple('Boundary', ['min_x', 'max_x', 'min_y', 'max_y', 'min_z', 'max_z'])

class SimulatedObject(object):
    """
    Simulated object that moves on a circular path in front of the robot.
    The path is contained in a plane parallel to the y-z plane (i.e., x is constant for all points in the path).
    """
    def __init__(self, boundary=None):
        """
        Constructor
        """
        self.x = 1.5             # constant x coordinate for the object
        self.y = 0.0             # y coordinate for the center of the object's path
        self.z = 0.50            # z coordinate for the center of the object's path
        self.roll = 0.0
        self.pitch = 0.0
        self.yaw = 0.0

        self.radius = 0.1               # radius of the object's circular path
        self.frame = "base_footprint"   # frame in which the coordinates of the object are computed

        if isinstance(boundary, Boundary):
            self.boundary = boundary

    def update(self, dt):
        pass

    def update_from_pose_msg(self, pose_msg):
        self.x = pose_msg.pose.position.x
        self.y = pose_msg.pose.position.y
        self.z = pose_msg.pose.position.z
        target_angles = euler_from_quaternion([pose_msg.pose.orientation.x,
                                               pose_msg.pose.orientation.y,
                                               pose_msg.pose.orientation.z,
                                               pose_msg.pose.orientation.w])
        self.roll = target_angles[0]
        self.pitch = target_angles[1]
        self.yaw = target_angles[2]

    def construct_pose_msg(self):
        orientation = quaternion_from_euler(self.roll, self.pitch, self.yaw)
        pose_msg = PoseStamped()
        pose_msg.header.stamp = rospy.Time.now()
        pose_msg.header.frame_id = self.frame
        pose_msg.pose.position.x = self.x
        pose_msg.pose.position.y = self.y
        pose_msg.pose.position.z = self.z
        pose_msg.pose.orientation = Quaternion(orientation[0],
                                               orientation[1],
                                               orientation[2],
                                               orientation[3])
        return pose_msg

    def construct_marker_msg(self, pose_msg=None):
        if pose_msg is None:
            pose_msg = self.construct_pose_msg()
        marker_msg = Marker()
        marker_msg.header = pose_msg.header
        marker_msg.action = Marker.ADD
        marker_msg.color.a = 0.5
        marker_msg.color.r = 1.0
        marker_msg.lifetime = rospy.Duration(1.0)
        marker_msg.id = 23990426
        marker_msg.ns = "target"
        marker_msg.type = Marker.SPHERE
        marker_msg.pose = pose_msg.pose
        marker_msg.scale.x = 0.1
        marker_msg.scale.y = 0.1
        marker_msg.scale.z = 0.1
        return marker_msg

    def check_bounds(self):
        if self.boundary is None:
            return True
        if self.x < self.boundary.min_x or self.x > self.boundary.max_x:
            return False
        if self.y < self.boundary.min_y or self.y > self.boundary.max_y:
            return False
        if self.z < self.boundary.min_z or self.z > self.boundary.max_z:
            return False
        return True

    def clip_to_bounds(self):
        if self.boundary is None:
            return
        self.x = np.clip(self.x, self.boundary.min_x, self.boundary.max_x)
        self.y = np.clip(self.y, self.boundary.min_y, self.boundary.max_y)
        self.z = np.clip(self.z, self.boundary.min_z, self.boundary.max_z)


class TeleopTargetNode():

    PS3_AXIS_STICK_LEFT_LEFTWARDS = 0
    PS3_AXIS_STICK_LEFT_UPWARDS = 1
    PS3_AXIS_STICK_RIGHT_UPWARDS = 4
    PS3_L1 = 4
    PS3_R1 = 5

    def __init__(self):
        """
        Main function. Publishes the target at a constant frame rate.
        """
        # Create the simulated object
        self.object = SimulatedObject()

        # Init the node
        rospy.init_node('generate_target', anonymous=True)

        # Get ROS params
        x_value = rospy.get_param("~x_value", default=1.5)
        self.object.x = x_value

        # Define publishers
        vector_pub = rospy.Publisher('/target', PoseStamped, queue_size=5)
        marker_pub = rospy.Publisher('/target_marker', Marker, queue_size=5)
        self.move_pub = rospy.Publisher('/move_towards_target', Bool, queue_size=5, latch=True)

        # Define subscribers
        rospy.Subscriber('joy', Joy, self.joy_callback, queue_size=5)

        hz = 15
        rate = rospy.Rate(hz)
        while not rospy.is_shutdown():
            # publish the location of the target as a Vector3Stamped
            pose_msg = self.object.construct_pose_msg()
            vector_pub.publish(pose_msg)

            # publish a marker to visualize the target in RViz
            marker_msg = self.object.construct_marker_msg(pose_msg)
            marker_pub.publish(marker_msg)

            self.object.update(1.0/hz)

            # sleep to keep the desired publishing rate
            rate.sleep()

    def joy_callback(self, msg):
        """
        Joy Callback
        :param msg: joy message
        """
        # get commands
        scale = 0.02
        self.object.z += msg.axes[TeleopTargetNode.PS3_AXIS_STICK_LEFT_UPWARDS] * scale
        self.object.y += msg.axes[TeleopTargetNode.PS3_AXIS_STICK_LEFT_LEFTWARDS] * scale
        self.object.x += msg.axes[TeleopTargetNode.PS3_AXIS_STICK_RIGHT_UPWARDS] * scale

        if msg.buttons[TeleopTargetNode.PS3_L1] == 1:
            b = Bool()
            b.data = True
            self.move_pub.publish(b)
        elif msg.buttons[TeleopTargetNode.PS3_R1] == 1:
            b = Bool()
            b.data = False
            self.move_pub.publish(b)


if __name__ == '__main__':
    try:
        TeleopTargetNode()
    except rospy.ROSInterruptException:
        pass
