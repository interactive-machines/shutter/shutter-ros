search_mode=OPTIMIZE_MAX_JOINT
srdf_filename=shutter.srdf
robot_name_in_srdf=shutter
moveit_config_pkg=shutter_moveit_config
robot_name=shutter
planning_group_name=shutter
ikfast_plugin_pkg=shutter_ikfast_plugin
base_link_name=base_link
eef_link_name=camera_link
ikfast_output_path=/home/al2242/tarzan_ws/src/shutter-ros/shutter_ikfast_plugin/src/shutter_ikfast_solver.cpp

rosrun moveit_kinematics create_ikfast_moveit_plugin.py\
  --search_mode=$search_mode\
  --srdf_filename=$srdf_filename\
  --robot_name_in_srdf=$robot_name_in_srdf\
  --moveit_config_pkg=$moveit_config_pkg\
  $robot_name\
  $planning_group_name\
  $ikfast_plugin_pkg\
  $base_link_name\
  $eef_link_name\
  $ikfast_output_path
