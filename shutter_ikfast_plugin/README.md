# Shutter IKFast Plugin

This package contains the [IKFast](http://openrave.org/docs/latest_stable/openravepy/ikfast/) inverse kinematics solver and MoveIt plugin for Shutter.
The solver was generated with OpenRAVE following the [MoveIt IKFast tutorial](https://ros-planning.github.io/moveit_tutorials/doc/ikfast/ikfast_tutorial.html).

In particular, five specific steps were modified for Shutter:

1. URDF is located at `shutter_description/robots/generated_urdf/shutter.v.3.0.urdf`
2. Planning group is `shutter_arm`
3. IK type is `TranslationZAxisAngle4D`
4. Base link is `base_link`
5. End effector link is `camera_link`

After setting up the shutter-ros repository and following the steps in the IKFast tutorial, the command to generate the solver for Shutter is:

```bash
$ rosrun moveit_kinematics auto_create_ikfast_moveit_plugin.sh --name shutter --pkg shutter_ikfast_plugin --iktype TranslationZAxisAngle4D $(rospack find shutter_description)/robots/generated_urdf/shutter.v.3.0.urdf shutter_arm base_link camera_link
```

Additionally, the names of the generated source files and C++ classes were modified from `shutter_shutter_arm` (following a naming convention of `<robot>_<planning_group>`) to `shutter`.

Note that IKFast has several [open issues](http://openrave.org/docs/latest_stable/openravepy/ikfast/#open-issues), and was designed for kinematic chains more sophisticated than Shutter.
Hence, other inverse kinematics solvers such as TRAC-IK are generally better choices.