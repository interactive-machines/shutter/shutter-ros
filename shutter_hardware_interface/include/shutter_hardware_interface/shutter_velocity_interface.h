#include <memory>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>

#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_interface.h>

#include "shutter_hardware_interface/dynamixel_ros_control.h"

class ShutterVelocityInterface : public hardware_interface::RobotHW {
public:
  ShutterVelocityInterface(std::shared_ptr<DynamixelController>& dxl_wb_ptr);

  void read();
  void write(const bool enforce_limits = true);
  bool jog();

private:
  std::shared_ptr<DynamixelController> dynamixel_controller_;
  hardware_interface::JointStateInterface jnt_state_interface_;
  hardware_interface::VelocityJointInterface jnt_vel_interface_;
  joint_limits_interface::VelocityJointSoftLimitsInterface jnt_limits_interface_;
  double cmd_[4] = {0.0, 0.0, 0.0, 0.0};
  double pos_[4] = {0.0, 0.0, 0.0, 0.0};
  double vel_[4] = {0.0, 0.0, 0.0, 0.0};
  double eff_[4] = {0.0, 0.0, 0.0, 0.0};
};
