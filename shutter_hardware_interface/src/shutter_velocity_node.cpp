#include <memory>

#include <ros/ros.h>
#include <controller_manager/controller_manager.h>

#include "shutter_hardware_interface/shutter_velocity_interface.h"
#include "shutter_hardware_interface/dynamixel_ros_control.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "shutter_hardware_interface");
  ros::NodeHandle nh;

  std::string port_name = "/dev/ttyUSB0";
  uint32_t baud_rate = 4000000;

  if (argc < 2)
  {
    ROS_FATAL_STREAM("Please set '-port_name' and  '-baud_rate' arguments for connected Dynamixels");
    ros::shutdown();
    return 0;
  }
  else
  {
    port_name = argv[1];
    baud_rate = atoi(argv[2]);
  }

  auto dynamixel_controller = std::make_shared<DynamixelController>();

  ros::AsyncSpinner spinner(4);
  spinner.start();

  std::string yaml_file = nh.param<std::string>("dynamixel_info", "");
  if (!dynamixel_controller->initWorkbench(port_name, baud_rate))
  {
    ROS_FATAL_STREAM("Please check USB port name");
    ros::shutdown();
  }

  if (!dynamixel_controller->getDynamixelsInfo(yaml_file))
  {
    ROS_FATAL_STREAM("Please check YAML file");
    ros::shutdown();
  }

  while (!dynamixel_controller->loadDynamixels())
  {
    ROS_ERROR_STREAM_THROTTLE(1, "Please check Dynamixel ID or BaudRate");
  }

  if (!dynamixel_controller->initDynamixels())
  {
    ROS_FATAL_STREAM("Please check control table (http://emanual.robotis.com/#control-table)");
    ros::shutdown();
  }

  if (!dynamixel_controller->initControlItems())
  {
    ROS_FATAL_STREAM("Please check control items");
    ros::shutdown();
  }

  if (!dynamixel_controller->initSDKHandlers())
  {
      ROS_FATAL_STREAM("Failed to set Dynamixel SDK Handler");
      ros::shutdown();
  }

  dynamixel_controller->initPublisher();

  ShutterVelocityInterface shutter(dynamixel_controller);
  auto cm = std::make_shared<controller_manager::ControllerManager>(&shutter);

  ros::Rate loop_rate(100.0);
  ros::Time last_time = ros::Time::now();
  ros::Time current_time = ros::Time::now();
  ros::Duration elapsed_time;

  ros::Rate jog_rate(5.0);
  shutter.read();
  while(!shutter.jog())
  {
    shutter.write(false);
    shutter.read();
    jog_rate.sleep();
  }

  while(ros::ok())
  {
    shutter.read();
    current_time = ros::Time::now();
    elapsed_time = ros::Duration(current_time - last_time);
    last_time = current_time;

    cm->update(ros::Time::now(), elapsed_time, false);

    shutter.write();

    // Publishes to /joint_states
    dynamixel_controller->publishCallback();
    loop_rate.sleep();
  }

  ros::waitForShutdown();
  return 0;
}
