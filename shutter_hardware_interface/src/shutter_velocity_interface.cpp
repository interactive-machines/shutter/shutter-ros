#include <algorithm>
#include "shutter_hardware_interface/shutter_velocity_interface.h"

ShutterVelocityInterface::ShutterVelocityInterface(std::shared_ptr<DynamixelController>& dxl_wb_ptr)
  : dynamixel_controller_(dxl_wb_ptr)
{
  // connect and register the joint state interface
  hardware_interface::JointStateHandle state_handle_1("joint_1", &pos_[0], &vel_[0], &eff_[0]);
  jnt_state_interface_.registerHandle(state_handle_1);

  hardware_interface::JointStateHandle state_handle_2("joint_2", &pos_[1], &vel_[1], &eff_[1]);
  jnt_state_interface_.registerHandle(state_handle_2);

  hardware_interface::JointStateHandle state_handle_3("joint_3", &pos_[2], &vel_[2], &eff_[2]);
  jnt_state_interface_.registerHandle(state_handle_3);

  hardware_interface::JointStateHandle state_handle_4("joint_4", &pos_[3], &vel_[3], &eff_[3]);
  jnt_state_interface_.registerHandle(state_handle_4);

  registerInterface(&jnt_state_interface_);

  // connect and register the joint velocity interface
  hardware_interface::JointHandle vel_handle_1(jnt_state_interface_.getHandle("joint_1"), &cmd_[0]);
  jnt_vel_interface_.registerHandle(vel_handle_1);

  hardware_interface::JointHandle vel_handle_2(jnt_state_interface_.getHandle("joint_2"), &cmd_[1]);
  jnt_vel_interface_.registerHandle(vel_handle_2);

  hardware_interface::JointHandle vel_handle_3(jnt_state_interface_.getHandle("joint_3"), &cmd_[2]);
  jnt_vel_interface_.registerHandle(vel_handle_3);

  hardware_interface::JointHandle vel_handle_4(jnt_state_interface_.getHandle("joint_4"), &cmd_[3]);
  jnt_vel_interface_.registerHandle(vel_handle_4);

  registerInterface(&jnt_vel_interface_);
  ROS_INFO("JointState and Velocity Interfaces registered.");

  // joint 1 URDF limits
  joint_limits_interface::JointLimits limits_joint_1_;
  limits_joint_1_.has_velocity_limits = true;
  limits_joint_1_.max_velocity = 0.785;
  limits_joint_1_.has_position_limits = true;
  limits_joint_1_.max_position = 2.617;
  limits_joint_1_.min_position = -2.617;

  // joint 1 soft limits
  joint_limits_interface::SoftJointLimits soft_limits_joint_1_;
  soft_limits_joint_1_.k_position = 1.0;
  soft_limits_joint_1_.max_position = 2.61;
  soft_limits_joint_1_.min_position = -2.61;

  // joint 2 URDF limits
  joint_limits_interface::JointLimits limits_joint_2_;
  limits_joint_2_.has_velocity_limits = true;
  limits_joint_2_.max_velocity = 1.571;
  limits_joint_2_.has_position_limits = true;
  limits_joint_2_.max_position = 1.571;
  limits_joint_2_.min_position = -1.571;

  // joint 2 soft limits
  joint_limits_interface::SoftJointLimits soft_limits_joint_2_;
  soft_limits_joint_2_.k_position = 5.0;
  soft_limits_joint_2_.max_position = 1.54;
  soft_limits_joint_2_.min_position = -1.54;

  // joint 3 URDF limits
  joint_limits_interface::JointLimits limits_joint_3_;
  limits_joint_3_.has_velocity_limits = true;
  limits_joint_3_.max_velocity = 1.571;
  limits_joint_3_.has_position_limits = true;
  limits_joint_3_.max_position = 1.571;
  limits_joint_3_.min_position = -1.571;

  // joint 3 soft limits
  joint_limits_interface::SoftJointLimits soft_limits_joint_3_;
  soft_limits_joint_3_.k_position = 5.0;
  soft_limits_joint_3_.max_position = 1.54;
  soft_limits_joint_3_.min_position = -1.54;

  // joint 4 URDF limits
  joint_limits_interface::JointLimits limits_joint_4_;
  limits_joint_4_.has_velocity_limits = true;
  limits_joint_4_.max_velocity = 1.571;
  limits_joint_4_.has_position_limits = true;
  limits_joint_4_.max_position = 1.571;
  limits_joint_4_.min_position = -1.571;

  // joint 4 soft limits
  joint_limits_interface::SoftJointLimits soft_limits_joint_4_;
  soft_limits_joint_4_.k_position = 1.0;
  soft_limits_joint_4_.max_position = 1.54;
  soft_limits_joint_4_.min_position = -1.54;

  // Register joint limits handles
  joint_limits_interface::VelocityJointSoftLimitsHandle joint_1_limits_handle(vel_handle_1, limits_joint_1_, soft_limits_joint_1_);
  joint_limits_interface::VelocityJointSoftLimitsHandle joint_2_limits_handle(vel_handle_2, limits_joint_2_, soft_limits_joint_2_);
  joint_limits_interface::VelocityJointSoftLimitsHandle joint_3_limits_handle(vel_handle_3, limits_joint_3_, soft_limits_joint_3_);
  joint_limits_interface::VelocityJointSoftLimitsHandle joint_4_limits_handle(vel_handle_4, limits_joint_4_, soft_limits_joint_4_);

  jnt_limits_interface_.registerHandle(joint_2_limits_handle);
  jnt_limits_interface_.registerHandle(joint_1_limits_handle);
  jnt_limits_interface_.registerHandle(joint_3_limits_handle);
  jnt_limits_interface_.registerHandle(joint_4_limits_handle);
}

void ShutterVelocityInterface::read()
{
  // needs to call DynamixelController readCallback and store in pos_, vel_, eff_
  dynamixel_controller_->readCallback();

  pos_[0] = dynamixel_controller_->pos_to_read[0];
  pos_[1] = dynamixel_controller_->pos_to_read[1];
  pos_[2] = dynamixel_controller_->pos_to_read[2];
  pos_[3] = dynamixel_controller_->pos_to_read[3];

  vel_[0] = dynamixel_controller_->vel_to_read[0];
  vel_[1] = dynamixel_controller_->vel_to_read[1];
  vel_[2] = dynamixel_controller_->vel_to_read[2];
  vel_[3] = dynamixel_controller_->vel_to_read[3];
}

void ShutterVelocityInterface::write(const bool enforce_limits)
{
  if (enforce_limits)
  {
    jnt_limits_interface_.enforceLimits(ros::Duration(0.1));
  }
  // needs to call DynamixelController writeCallback and apply cmd_
  dynamixel_controller_->writeVelocity(cmd_);
}

bool ShutterVelocityInterface::jog()
{
  cmd_[0] = 0.0;

  // if joints 2 and 3 are in unusable initial states, jog to 'resting'
  if (pos_[1] < -1.54)
    cmd_[1] = 0.5;
  else
    cmd_[1] = 0.0;

  if (pos_[2] < -1.54)
    cmd_[2] = 0.5;
  else
    cmd_[2] = 0.0;

  // also jog joint 4, otherwise head and shoulder links in collision
  if (pos_[3] < -0.1 && (pos_[1] < -1.54 || pos_[2] < -1.54))
    cmd_[3] = 0.5;
  else
    cmd_[3] = 0.0;

  return std::all_of(std::begin(cmd_), std::end(cmd_), [](double command){ return command == 0; });
}
