#include <ros/ros.h>
#include <controller_manager_msgs/ListControllers.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/trajectory_execution_manager/trajectory_execution_manager.h>
#include "shutter_teleop/shutter_teleop.h"

static const std::string LOGNAME = "shutter_teleop_node";

int main(int argc, char** argv)
{
  ros::init(argc, argv, LOGNAME);
  ros::NodeHandle nh("~");

  bool simulation = false;
  nh.param<bool>("simulation", simulation, false);

  // Block until the controller is available
  if (!simulation)
  {
    bool servo_controller_available = false;
    controller_manager_msgs::ListControllers srv;
    ros::ServiceClient list_controller_client = nh.serviceClient<controller_manager_msgs::ListControllers>("/controller_manager/list_controllers");
    list_controller_client.waitForExistence();
    do
    {
      list_controller_client.call(srv);
      for (auto controller_state : srv.response.controller)
      {
        if (controller_state.name == "velocity_group_controller")
          servo_controller_available = true;
      }
    } while (!servo_controller_available);
    list_controller_client.shutdown();
  }

  ros::AsyncSpinner spinner(4);
  spinner.start();

  // Load the planning scene monitor
  planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor;
  planning_scene_monitor = std::make_shared<planning_scene_monitor::PlanningSceneMonitor>("robot_description");
  if (!planning_scene_monitor->getPlanningScene())
  {
    ROS_ERROR_STREAM_NAMED(LOGNAME, "Error in setting up the PlanningSceneMonitor.");
    exit(EXIT_FAILURE);
  }

  planning_scene_monitor->startSceneMonitor();
  planning_scene_monitor->startWorldGeometryMonitor(
      planning_scene_monitor::PlanningSceneMonitor::DEFAULT_COLLISION_OBJECT_TOPIC,
      planning_scene_monitor::PlanningSceneMonitor::DEFAULT_PLANNING_SCENE_WORLD_TOPIC,
      false /* skip octomap monitor */);
  planning_scene_monitor->startStateMonitor();

  if (!simulation)
  {
    // Configure trajectory execution manager
    trajectory_execution_manager::TrajectoryExecutionManagerPtr trajectory_execution_manager;
    trajectory_execution_manager = std::make_shared<trajectory_execution_manager::TrajectoryExecutionManager>(planning_scene_monitor->getRobotModel(), planning_scene_monitor->getStateMonitor());

    while (!trajectory_execution_manager->ensureActiveController("/velocity_group_controller"))
    {
      ROS_DEBUG_STREAM_THROTTLE(1, "Unable to ensure /joint_group_controller is active, blocking!");
    }
  }

  // Create the servo object
  shutter_teleop::ShutterTeleop servo(nh, planning_scene_monitor, simulation);
  ros::waitForShutdown();

  return EXIT_SUCCESS;
}
