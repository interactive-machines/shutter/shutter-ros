#!/usr/bin/env python3
# Node to move the eyes on the robot.

import argparse
import rospy
from shutter_face_ros.msg import PupilsLocation
from sensor_msgs.msg import Joy
from std_msgs.msg import String


class FaceTeleopNode:
    """
    Moves the pupils according to the joystick messages.
    """

    def __init__(self, disable_pupils):
        # init node
        rospy.init_node('face_teleop_node', anonymous=False)

        # constants
        self.DISPLAY_FACTOR = 80
        self.pupils = not disable_pupils

        # Data keeper
        self.FACE = None
        self.EYES = None

        # Subscribers
        rospy.Subscriber('joy', Joy, self.callback, queue_size=1)

        # Publishers
        self.pupils_pub = rospy.Publisher("gaze/pupils_location", PupilsLocation, queue_size=1)
        self.expression_pub = rospy.Publisher("gaze/expression_index", String, queue_size=1)

        # do nothing.. just wait for gaze commands
        rospy.spin()

    def callback(self, joy_msg):

        # Create a PupilsLocation object:
        pupils_location = PupilsLocation()
        pupils_location.header = joy_msg.header

        # Uses the shape buttons and d-pad to receive requests to change facial expressions
        if joy_msg.buttons[0] == 1:
            self.expression_pub.publish('neutral')
        elif joy_msg.buttons[1] == 1:
            self.expression_pub.publish('angry')
        elif joy_msg.buttons[2] == 1:
            self.expression_pub.publish('bored')
        elif joy_msg.buttons[3] == 1:
            self.expression_pub.publish('determined')
        elif joy_msg.buttons[13] == 1:
            self.expression_pub.publish('happy')
        elif joy_msg.buttons[14] == 1:
            self.expression_pub.publish('happy2')
        elif joy_msg.buttons[15] == 1:
            self.expression_pub.publish('sad')
        elif joy_msg.buttons[16] == 1:
            self.expression_pub.publish('surprised')

        if self.pupils:
            pupils_location.left_eye.x = -joy_msg.axes[3] * self.DISPLAY_FACTOR
            pupils_location.left_eye.y = -joy_msg.axes[4] * self.DISPLAY_FACTOR
            pupils_location.right_eye.x = -joy_msg.axes[0] * self.DISPLAY_FACTOR
            pupils_location.right_eye.y = -joy_msg.axes[1] * self.DISPLAY_FACTOR

        # Publish into the eyes
        self.pupils_pub.publish(pupils_location)
        self.EYES = [pupils_location.left_eye, pupils_location.right_eye]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--disable_pupils', action='store_true', help='Disable joystick control of pupils')
    args = parser.parse_args(rospy.myargv()[1:])
    try:
        FaceTeleopNode(disable_pupils=args.disable_pupils)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
