#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

static const std::string LOGNAME = "soft_timeout_node";
static const int NUM_SPINNERS = 1;

class SoftTimeout
{
public:
  SoftTimeout() : spinner_(NUM_SPINNERS)
  {
    soft_timeout_ = nh_.createTimer(ros::Duration(0.1), &SoftTimeout::softTimeoutCallback, this, false, true);
    command_sub_ = nh_.subscribe("/velocity_group_controller/servo_command", 1, &SoftTimeout::commandCallback, this);
    command_pub_ = nh_.advertise<std_msgs::Float64MultiArray>("/velocity_group_controller/command", 1);

    spinner_.start();
    ros::waitForShutdown();
  };

private:
  void softTimeoutCallback(const ros::TimerEvent&)
  {
    std_msgs::Float64MultiArray zero_velocity;
    zero_velocity.data = {0.0, 0.0, 0.0, 0.0};
    command_pub_.publish(zero_velocity);
  }

  void commandCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
  {
    soft_timeout_.stop();
    command_pub_.publish(msg);
    soft_timeout_.start();
  }

  ros::NodeHandle nh_;
  ros::Timer soft_timeout_;
  ros::Subscriber command_sub_;
  ros::Publisher command_pub_;
  ros::AsyncSpinner spinner_;
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, LOGNAME);

  SoftTimeout soft_timeout;
  return 0;
}
