#include <algorithm>
#include <boost/chrono.hpp>
#include <tf2_eigen/tf2_eigen.h>
#include "shutter_teleop/shutter_teleop.h"

static const std::string LOGNAME = "shutter_teleop";

namespace shutter_teleop
{
ShutterTeleop::ShutterTeleop(const ros::NodeHandle& nh,
                       const planning_scene_monitor::PlanningSceneMonitorPtr& planning_scene_monitor,
                       const bool simulation)
  : nh_(nh)
  , planning_scene_monitor_(planning_scene_monitor)
  , transform_listener_(transform_buffer_)
{
  if (simulation)
  {
    sim_scaling_ = 3;
  }

  // Use the C++ interface that Servo provides
  tracker_ = std::make_unique<moveit_servo::PoseTracking>(nh_, planning_scene_monitor_);

  // Setup ROS interfaces
  target_pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("target_pose", 1, true);
  joy_sub_ = nh_.subscribe("/joy", 1, &ShutterTeleop::joyCallback, this);
  servo_status_sub_ = nh_.subscribe("servo/status", 1, &ShutterTeleop::status_callback, this);
}

void ShutterTeleop::start()
{
  tracker_->servo_->setPaused(false);
  tracker_->resetTargetPose();
  move_to_pose_thread_ = std::make_unique<boost::thread>(
    [this] {
      boost::this_thread::interruption_point();
      while (tracker_->moveToPose(lin_tol, rot_tol, 0.1) != moveit_servo::PoseTrackingStatusCode::STOP_REQUESTED)
      {
        boost::this_thread::interruption_point();
      }
    });
}

void ShutterTeleop::stop()
{
  tracker_->servo_->setPaused(true);
  if (!move_to_pose_thread_)
  {
    return;
  }

  if (move_to_pose_thread_->joinable())
  {
    while (!move_to_pose_thread_->try_join_for(boost::chrono::milliseconds(10)))
    {
      tracker_->stopMotion();
      move_to_pose_thread_->interrupt();
    }
    move_to_pose_thread_.reset(nullptr);
  }
}

void ShutterTeleop::joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
  // Check if all zeros
  if (std::all_of(msg->axes.begin(), msg->axes.end(), [](double cmd){ return cmd == 0.0; }))
  {
    stop();
    return;
  }
  else
  {
    if (!move_to_pose_thread_)
    {
      start();
    }
  }

  geometry_msgs::PoseStamped target_pose;
  target_pose.header.frame_id = "base_link";

  // lookup current pose
  auto current_state = planning_scene_monitor_->getStateMonitor()->getCurrentState();
  const Eigen::Isometry3d& camera_link_pose = current_state->getGlobalLinkTransform("camera_link");

  target_pose.pose = tf2::toMsg(camera_link_pose);
  try
  {
    geometry_msgs::TransformStamped base_to_planning_frame = transform_buffer_.lookupTransform("head_link", "base_link", ros::Time(0), ros::Duration(0.1));
    tf2::doTransform(target_pose, target_pose, base_to_planning_frame);
  }
  catch (const tf2::TransformException& ex)
  {
    ROS_WARN_STREAM_THROTTLE(1, ex.what());
    return;
  }
  target_pose.pose.position.y += (msg->axes[4] * linear_scaling_ * sim_scaling_);
  target_pose.pose.position.z += (msg->axes[3] * linear_scaling_ * sim_scaling_);

  tf2::Quaternion q_orig, q_rot, q_new;
  tf2::convert(target_pose.pose.orientation, q_orig);
  q_rot.setRPY(msg->axes[1] * angular_scaling_ * sim_scaling_* -1, msg->axes[0] * angular_scaling_ * sim_scaling_, 0.0);
  q_new = q_rot * q_orig;
  q_new.normalize();
  tf2::convert(q_new, target_pose.pose.orientation);

  tracker_->resetTargetPose();
  target_pose.header.stamp = ros::Time::now();
  target_pose_pub_.publish(target_pose);
  /*
  if (!(status_ == moveit_servo::StatusCode::HALT_FOR_SINGULARITY
        || status_ == moveit_servo::StatusCode::HALT_FOR_COLLISION))
  {
    target_pose_pub_.publish(target_pose);
    return;
  }
  */
}

void ShutterTeleop::status_callback(const std_msgs::Int8ConstPtr& msg)
{
  moveit_servo::StatusCode latest_status = static_cast<moveit_servo::StatusCode>(msg->data);
  if (latest_status != status_)
  {
    status_ = latest_status;
    const auto& status_str = moveit_servo::SERVO_STATUS_CODE_MAP.at(status_);
    ROS_DEBUG_STREAM_THROTTLE(1, "Servo status: " << status_str);
  }
}
} // namespace shutter_teleop
