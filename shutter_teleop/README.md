# Shutter Teleop Package

Code and scripts for teleoperating shutter through a computer or a joystick.

## Controlling the face

You can use a controller to move the robot's pupils around by executing the following:

```console
$ roslaunch shutter_teleop face_controller.launch
```

The left pupil will mirror the movements of the left joystick, and the right pupil will mirror the movements of the right joystick. If it's your first time using a controller for ROS, you may have to install/configure the joy node by following [these instructions.](http://wiki.ros.org/joy/Tutorials/ConfiguringALinuxJoystick)

Additionally, you can use the shape buttons and d-pad to change the facial expression of the robot. Each button maps to one of eight expressions.

The face can also be controlled without a physical robot:

```console
$ roslaunch shutter_teleop face_controller.launch move_to_shutter_screen:=false
```

## Moving the Robot with a Controller

The robot can be operated by a controller by executing the following:

```console
$ roslaunch shutter_teleop shutter_controller.launch
```

The robot can be moved now via the Joy node. Commands from the joystick (e.g., a PlayStation3 controller) modify the goal pose for the robot in the `head_link` frame. This frame is the same as the one for the MoveIt handles in RViz. The following mapping, however, references the commands relative to the axes of the `base_link` frame. Moving the left stick horizontally will change the head's yaw (corresponding to joint 1), and moving the left stick vertically will change the head's pitch (corresponding to joint 4). Moving the right stick vertically will move the head up and down (along the z-axis), and moving the right stick horizontally will move the head forward and backward (along the x-axis). Give the robot a few seconds to load after launching before operating.

The calculations for joint positions are computed with the pose tracking interface to MoveIt Servo. API documentation for MoveIt Servo is located here: [http://docs.ros.org/en/noetic/api/moveit_servo/html/index.html](http://docs.ros.org/en/noetic/api/moveit_servo/html/index.html)

The robot can also be teleoperated in Unity simulation:

```console
$ roslaunch shutter_teleop shutter_controller.launch simulation:=true
```

For convenience, a configuration for RViz is provided to visualize the robot model and goal pose for the robot.

```console
$ roslaunch shutter_teleop shutter_controller.launch rviz:=true
```

![Shutter tracking a pose controlled by a joystick](../images/shutter_teleop_rviz.gif){w=300px align=center}
