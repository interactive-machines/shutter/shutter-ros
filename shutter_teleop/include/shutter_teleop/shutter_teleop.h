#include <memory>
#include <boost/thread/thread.hpp>

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <std_msgs/Int8.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Joy.h>
#include <moveit_servo/pose_tracking.h>
#include <moveit_servo/status_codes.h>

namespace shutter_teleop
{
class ShutterTeleop
{
public:
  ShutterTeleop(const ros::NodeHandle& nh,
             const planning_scene_monitor::PlanningSceneMonitorPtr& planning_scene_monitor,
             const bool simulation);

  void start();
  void stop();

private:
  planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor_;
  std::unique_ptr<moveit_servo::PoseTracking> tracker_;
  std::unique_ptr<boost::thread> move_to_pose_thread_;

  const Eigen::Vector3d lin_tol = { 0.01, 0.01, 0.01 };
  const double rot_tol = 0.1;
  const double linear_scaling_ = 0.07;
  const double angular_scaling_ = 0.3;
  int sim_scaling_ = 1;

  moveit_servo::StatusCode status_ = moveit_servo::StatusCode::INVALID;

  ros::NodeHandle nh_;
  ros::Publisher target_pose_pub_;
  ros::Subscriber joy_sub_;
  ros::Subscriber servo_status_sub_;

  tf2_ros::Buffer transform_buffer_;
  tf2_ros::TransformListener transform_listener_;

  void joyCallback(const sensor_msgs::Joy::ConstPtr& msg);
  void status_callback(const std_msgs::Int8ConstPtr& msg);
};
}
